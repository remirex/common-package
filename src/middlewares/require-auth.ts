import {NextFunction, Request, Response} from 'express';
import {NotAuthorizedError, NotAllowed} from "../errors/not-authorized-error";

export const requireAuth = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    if (!req.currentUser) {
        throw new NotAuthorizedError();
    }

    next();
};

export const checkRole = (roles: string[]) => {
    return (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        if (!roles.includes(req.currentUser!.role)) {
            throw new NotAllowed();
        }

        next();
    }
}